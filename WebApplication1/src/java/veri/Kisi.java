/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package veri;

/**
 *
 * @author User
 */
public class Kisi {
    String isim;
    String sifre;

    public Kisi(){//constructor- kurucu
        System.out.println("kisi objesi olusturuldu");
    }
    
    public boolean sifreKontrol(){
        DBKatmani dbk = new DBKatmani();
        return dbk.kullaniciKontrol(isim, sifre);
    }
    
    public String getSifre() {
        return sifre;
    }

    public void setSifre(String sifre) {
        this.sifre = sifre;
    }
    
    public String getIsim() {
        return isim;
    }

    public void setIsim(String isim) {
        this.isim = isim;
    }
    
    public static void main(String args[]){
        Kisi k = new Kisi();
        k.setIsim("salih");
        k.setSifre("123");
        System.out.println(k.sifreKontrol());
    }
}
